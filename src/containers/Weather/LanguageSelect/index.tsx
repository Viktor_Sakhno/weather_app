import React, {FC} from 'react';
import {observer} from 'mobx-react';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import {useStore} from '../../../store';
import {LangType} from '../../../models/language';

import styles from './styles.module.scss';

export const LanguageSelect: FC = observer(() => {
	const {language} = useStore();

	const handleChangeLanguage = (event: React.ChangeEvent<{ value: unknown }>) => {
		if (language.language === event.target.value) return;
		language.setLanguage(event.target.value as LangType);
	};

	return (
		<Select
			disableUnderline={true}
			value={language.language}
			onChange={handleChangeLanguage}
			classes={{root: styles.select, select: styles.selectedItem}}
			className={styles.languageSelect}
		>
			<MenuItem value='en'>EN</MenuItem>
			<MenuItem value='ua'>UA</MenuItem>
			<MenuItem value='ru'>RU</MenuItem>
		</Select>
	);
})
