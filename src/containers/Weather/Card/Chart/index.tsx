import React, {FC} from 'react';
import {observer} from 'mobx-react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

import {useStore} from '../../../../store';
import {getTime} from '../../../../utils/getTime';
import {ICityWeather} from '../../../../models/weather';

interface IProps {
	data: ICityWeather
}

export const Chart: FC<IProps> = observer(({data}) => {
	const {language} = useStore();
	const data24hours = data.hourly.slice(0, 24);

	const getCategoriesData = () => {
		return data24hours.map(item => getTime(item.dt, language.dictionary?.locale, 'time'))
	}

	const getSeriesData = () => {
		return data24hours.map(item => Math.round(item.temp))
	}

	const getAreaColor = () => {
		const sum = data24hours.reduce((acc, item) => acc + item.temp, 0);
		return (sum / 24 > 0) ? 'rgb(255,208,212)' : 'rgb(208,213,254)';
	}

	const categories = getCategoriesData();

	const chartOptions = {
		chart: {
			type: 'areaspline',
			backgroundColor: 'lavenderblush',
			scrollablePlotArea: {
				minWidth: 1200
			},
			paddingTop: 50,
			height: 170,
		},
		credits: {
			enabled: false
		},
		title: {
			text: null
		},
		xAxis: {
			min: 0.3,
			max: categories.length - 1.3,
			startOnTick: false,
			endOnTick: false,
			title: {
				text: null
			},
			lineWidth: 0,
			categories: categories
		},
		yAxis: {
			visible: false,
			title: {
				text: null
			}
		},
		plotOptions: {
			areaspline: {
				marker: {
					enabled: false
				},
				enableMouseTracking: false
			},
			series: {
				dataLabels: {
					enabled: true,
					style: {
						fontSize: 10
					}
				}
			},
		},
		series: [{
			data: getSeriesData(),
			lineColor: 'transparent',
			color: getAreaColor(),
			fillOpacity: 0.5,
			showInLegend: false,
		}]
	};

	return (
		<HighchartsReact
			highcharts={Highcharts}
			options={chartOptions}
		/>
	);
})
