import React, {FC, useEffect, useState} from 'react';
import {observer} from 'mobx-react';
import cn from 'classnames';
import {Chart} from './Chart';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';

import {useStore} from '../../../store';
import {getTime} from '../../../utils/getTime';
import {ICityWeather, OutInfoWeatherType} from '../../../models/weather';

import Mist from 'assets/image/mist.svg';
import ClearSky from 'assets/image/clearSky.svg';
import FewClouds from 'assets/image/fewClouds.svg';
import LightRain from 'assets/image/lightRain.svg';
import RainAndSnow from 'assets/image/rainAndSnow.svg';
import BrokenClouds from 'assets/image/brokenClouds.svg';
import ScatteredClouds from 'assets/image/scatteredClouds.svg';

import styles from './styles.module.scss';

interface IProps {
	data: ICityWeather
}

export const WeatherCard: FC<IProps> = observer(({data}) => {
	const {weather, language} = useStore();
	const [countryName, setCountryName] = useState<string>(data.country.name);

	useEffect(() => {
		translateCityName();
	}, [language.language]);

	const translateCityName = async () => {
		const result = await language.translateText(data.country.name, language.language);
		setCountryName(result);
	}

	const getImage = (value: string) => {
		switch (value) {
			case "clear sky":
				return ClearSky;
			case "overcast clouds":
			case "broken clouds":
				return BrokenClouds;
			case "few clouds":
				return FewClouds;
			case "moderate rain":
			case "light rain":
				return LightRain;
			case "scattered clouds":
				return ScatteredClouds;
			case "light snow":
			case "rain and snow":
				return RainAndSnow;
			case "mist":
				return Mist;
			default:
				return ClearSky;
		}
	}

	const deleteHandler = () => {
		weather.deleteCity(data.id);
		weather.deleteCityWeather(data.id);
	}

	const switchHandler = (units: string) => {
		if (data.units === units) return;
		weather.changeUnitsCity(data.id, units);
		const searchData = {
			id: data.id,
			lat: data.lat,
			lon: data.lon,
			units,
			country: {
				name: data.country.name,
				code: data.country.code
			}
		}
		weather.changeCityWeather(searchData);
	}

	const indexOutInfo: OutInfoWeatherType = data.current.weather[0].main as OutInfoWeatherType;

	return (
		<div className={styles.card}>
			<IconButton className={styles.deleteBtn} size='small' onClick={deleteHandler}>
				<CloseIcon fontSize='small'/>
			</IconButton>
			<div className={styles.title}>
				<div className={styles.cityName}>{`${countryName}, ${data.country.code}`}</div>
				<div className={styles.imgWeather}>
					<img src={getImage(data.current.weather[0].description)} alt="img"/>
					<div>{language.dictionary?.outInfo[indexOutInfo]}</div>
				</div>
			</div>
			<div className={styles.time}>
				{getTime(data.current.dt, language.dictionary?.locale)}
			</div>
			<Chart data={data}/>
			<div className={styles.info}>
				<div className={styles.infoLeft}>
					<div className={styles.switchBox}>
						<div className={styles.temp}>{Math.round(data.current.temp)}</div>
						<div className={styles.switch}>
							<div className={styles.switchC}>
								<div className={cn({[styles.switchDisabled]: data.units === 'imperial'})}
										 onClick={() => switchHandler('metric')}
								>°C
								</div>
							</div>
							<div className={styles.switchF}>
								<div className={cn({[styles.switchDisabled]: data.units === 'metric'})}
										 onClick={() => switchHandler('imperial')}
								>°F
								</div>
							</div>
						</div>
					</div>
					<div className={styles.feels}>
						{`${language.dictionary?.info.feelsLike} ${Math.round(data.current.feels_like)} ${data.units === 'metric' ? '°C' : '°F'}`}
					</div>
				</div>
				<div className={styles.infoRight}>
					<div>{language.dictionary?.info.wind}<span>{` ${Math.round(data.current.wind_speed)} ${data.units === 'metric' ? 'm/s' : 'mph'}`}</span>
					</div>
					<div>{language.dictionary?.info.humidity}<span>{` ${data.current.humidity} %`}</span></div>
					<div>{language.dictionary?.info.pressure}<span>{` ${data.current.pressure} Pa`}</span></div>
				</div>
			</div>
		</div>
	);
})
