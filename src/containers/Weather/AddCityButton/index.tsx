import React, {FC} from 'react';
import {observer} from 'mobx-react';
import Button from '@material-ui/core/Button';

import {useStore} from '../../../store';
import {ICity} from '../../../models/weather';

import styles from './styles.module.scss';

interface IProps {
	city: ICity | null,
	setCity: (city: ICity | null) => void,
	setValue: (value: string) => void,
	setRenderKey: () => void,
}

export const AddCityButton: FC<IProps> = observer(({setValue, city, setCity, setRenderKey}) => {
	const {language, notification, weather} = useStore();

	const btnHandler = () => {
		if (!city) {
			notification.setMessageNotification('City not selected!');
			notification.setTypeNotification('error');
			notification.openNotification();
			return;
		}

		if (weather.cities.find(item => item.id === city.id)) {
			setCity(null);
			setValue('');
			setRenderKey();
			notification.setMessageNotification('This city has already been added!');
			notification.setTypeNotification('error');
			notification.openNotification();
			return;
		}

		const cityFull = {...city, ...{units: 'metric'}};
		weather.addCity(cityFull);
		weather.getCityWeather(cityFull);
		setCity(null);
		setValue('');
		setRenderKey();
	}

	return (
		<div className={styles.btn}>
			<Button onClick={btnHandler}
							variant="contained"
							color="primary"
							classes={{root: styles.btnRoot}}
			>
				{language.dictionary?.searchBtn.label}
			</Button>
		</div>
	);
})
