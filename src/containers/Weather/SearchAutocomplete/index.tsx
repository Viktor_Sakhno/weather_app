import React, {FC, useState} from 'react';
import {observer} from 'mobx-react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { useDebouncedCallback } from 'use-debounce';

import {useStore} from '../../../store';
import {ICity} from '../../../models/weather';

import styles from './styles.module.scss';

interface IAutocomplete {
	renderKey: number,
	value: string,
	setValue: (value: string) => void,
	setCity: (city: ICity | null) => void
}

export const SearchAutocomplete: FC<IAutocomplete> = observer(({renderKey, value, setValue, setCity}) => {
	const {language, weather, notification} = useStore();
	const [cities, setCities] = useState<ICity[]>([]);

	const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		debouncedOnChange(e.target.value);
	}

	const debouncedOnChange = useDebouncedCallback(
		(value) => {
			setValue(value);
			value.length > 2 && getCitiesData(value);
		}, 250
	);

	const autocompleteOnChange = (event: React.ChangeEvent<{}>, value: ICity | null | string) => {
		setCity(value as ICity);
	};

	const getCitiesData = async (cityName: string) => {
		try {
			const response = await weather.getCities(cityName);
			if (response?.data.list.length) {
				setCities(response.data.list);
			}
		} catch (e) {
			notification.setMessageNotification(e.message);
			notification.setTypeNotification('error');
			notification.openNotification();
			console.log(e);
		}
	}

	const textFieldLabel = language.dictionary?.search.label;

	return (
		<Autocomplete
			key={renderKey}
			freeSolo
			autoComplete={true}
			onChange={autocompleteOnChange}
			filterOptions={(options, state) => options}
			options={cities}
			getOptionLabel={(option) => `${option.name}, ${option.sys.country}`}
			classes={{
				root: styles.autocomplete,
				inputRoot: styles.inputRoot
			}}
			renderOption={option => {
				const name = `${option.name}, ${option.sys.country}`;
				return (
					<>
						<img alt={name}
								 src={`https://openweathermap.org/images/flags/${option.sys.country.toLowerCase()}.png`}/>
						<span style={{marginLeft: '10px'}}>{name}</span>
					</>
				);
			}}
			renderInput={(params) => (
				<TextField
					{...params}
					label={textFieldLabel}
					variant="outlined"
					value={value}
					onChange={onChange}
				/>
			)}
		/>
	);
})
