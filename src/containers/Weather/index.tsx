import React, {FC, useEffect, useState} from 'react';
import {observer} from 'mobx-react';
import {WeatherCard} from './Card';
import {AddCityButton} from './AddCityButton';
import {LanguageSelect} from './LanguageSelect';
import {Notification} from 'components/Notification'
import {SearchAutocomplete} from './SearchAutocomplete';

import {useStore} from '../../store';
import {ICity} from '../../models/weather';

import styles from './styles.module.scss';


export const Weather: FC = observer(() => {
	const {weather, notification} = useStore();
	const [key, setKey] = useState<number>(1);
	const [value, setValue] = useState<string>('');
	const [city, setCity] = useState<ICity | null>(null);

	useEffect(() => {
		navigator.geolocation.getCurrentPosition(success, error, {timeout: 2000});
	}, []);

	const setRenderKey = () => {
		setKey(prev => prev + 1);
	}

	const getCityWeatherByCoord = async (lat: number, lon: number) => {
		const response = await weather.getCityByCoord(lat, lon);
		if (response.data.list.length) {
			weather.getCityWeather({...response.data.list[0], units: 'metric'});
		}
	}

	const handleCloseNotification = () => {
		notification.closeNotification();
	};


	const success = (position: GeolocationPosition) => {
		const latitude = position.coords.latitude;
		const longitude = position.coords.longitude;

		getCityWeatherByCoord(latitude, longitude);
	};

	const error = (err: GeolocationPositionError) => {
		console.warn(`ERROR(${err.code}): ${err.message}`);
	};

	return (
		<div className={styles.weatherContainer}>
			<Notification
				open={notification.notification}
				onClose={handleCloseNotification}
				type={notification.typeNotification}
				message={notification.messageNotification}
			/>
			<div className={styles.settingsContainer}>
				<div className={styles.search}>
					<SearchAutocomplete renderKey={key} value={value} setCity={setCity} setValue={setValue}/>
					<AddCityButton city={city} setRenderKey={setRenderKey} setValue={setValue} setCity={setCity}/>
				</div>
				<LanguageSelect/>
			</div>
			<div className={styles.cardsContainer}>
				{weather.citiesWeather.length
					? weather.citiesWeather.map(item => <WeatherCard key={item.id} data={item}/>)
					: null
				}
			</div>
		</div>
	);
});
