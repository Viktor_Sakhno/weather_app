import React, {useEffect} from 'react';
import {observer} from 'mobx-react';
import {LoadScript} from '@react-google-maps/api';
import {Router, Route, Switch} from 'react-router-dom';
import {Weather} from './containers/Weather';

import {useStore} from 'store';
import history from 'utils/history';
import {ICityFull} from './models/weather';
import {LangType} from './models/language';

import './App.css';

const App = observer(() => {
	const {language, weather} = useStore();

	useEffect(() => {
		const localLanguage = localStorage.getItem('weatherAppLanguage')
		if (localLanguage && language.language !== localLanguage) {
			language.setLanguage(localLanguage as LangType);
		} else {
			language.setDictionary();
		}
	}, [language.language]);

	useEffect(() => {
		const localCities: ICityFull[] | null = JSON.parse(
			String(localStorage.getItem('localCities'))
		)
		if (localCities) {
			weather.addLocalCities(localCities);
			localCities.forEach(city => weather.getCityWeather(city));
		}
	}, [])

	return (
		<LoadScript googleMapsApiKey='AIzaSyBnEp_gg8VFHPbd2QnFYX7VM5dcNFRzEsA'>
			<Router history={history}>
				<Switch>
					<Route path="/" component={Weather}/>
				</Switch>
			</Router>
		</LoadScript>
	);
})

export default App;
