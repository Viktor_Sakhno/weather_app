export const getTime = (value: number, locale: string = "en-GB", type?: string) => {
	const date = new Date(value * 1000);
	let formatter = new Intl.DateTimeFormat(locale, {
		weekday: 'short',
		day: 'numeric',
		month: 'long',
		hour: '2-digit',
		minute: '2-digit'
	});

	if (type === 'time') {
		formatter = new Intl.DateTimeFormat(locale, {
			hour: '2-digit',
			minute: '2-digit'
		});
	}
	return formatter.format(date);
}