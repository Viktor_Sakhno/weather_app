import React from 'react';
import {observer} from 'mobx-react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert, {Color} from '@material-ui/lab/Alert';

interface IProps {
	open: boolean;
	onClose: () => void;
	type?: Color;
	message: string;
}

export const Notification = observer(({open, onClose, type, message}: IProps) => {
	return (
		<Snackbar open={open}
							autoHideDuration={5000}
							onClose={onClose}
							anchorOrigin={{vertical: 'top', horizontal: 'right'}}>
			<MuiAlert elevation={1}
								severity={type}
								variant="filled"
								onClose={onClose}
			>
				{message}
			</MuiAlert>
		</Snackbar>
	);
})