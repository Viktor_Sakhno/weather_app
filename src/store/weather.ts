import {observable, action, makeAutoObservable} from 'mobx';
import { runInAction} from "mobx";

import store from 'store'
import {api} from '../utils/config';
import {ICityFull, ICityWeather, ISearchCityData} from '../models/weather';

class Weather {
	@observable cities: ICityFull[] = [];
	@observable citiesWeather: ICityWeather[] = [];

	constructor() {
		makeAutoObservable(this)
	}

	@action
	addCity(city: ICityFull) {
		const result = [...this.cities, city];
		this.cities = result;
		localStorage.setItem('localCities', JSON.stringify(result));
		store.notification.setMessageNotification('City successfully added!');
		store.notification.setTypeNotification('success');
		store.notification.openNotification();
	}

	@action
	addLocalCities(cities: ICityFull[]) {
		this.cities = cities;
	}

	@action
	deleteCity(id: number) {
		const result = this.cities.filter(item => item.id !== id);
		this.cities = result;
		localStorage.setItem('localCities', JSON.stringify(result));
	}

	@action
	changeUnitsCity(id: number, units: string) {
		const result = this.cities.map(city => {
			return city.id === id ? {...city, units} : city;
		});
		this.cities = result;
		localStorage.setItem('localCities', JSON.stringify(result));
	}

	@action
	async getCities(cityName: string) {
		return await api.get(`https://api.openweathermap.org/data/2.5/find?q=${cityName}&units=metric&appid=5367195d6e5bda5d4093665bfabfc217`);
	}

	@action
	async getCityByCoord(lat: number, lon: number) {
		return await api.get(`https://api.openweathermap.org/data/2.5/find?lat=${lat}&lon=${lon}&cnt=1&appid=5367195d6e5bda5d4093665bfabfc217`);
	}


	@action
	async getCityWeather(data: ICityFull) {
		try {
			const response = await api.get(
				`https://api.openweathermap.org/data/2.5/onecall?lat=${data.coord.lat}&lon=${data.coord.lon}&exclude=minutely,daily&units=${data.units}&appid=5367195d6e5bda5d4093665bfabfc217`
			)
			if (response) {
				const cityData = {
					...response.data, ...{
						id: data.id,
						country: {name: data.name, code: data.sys.country},
						units: data.units
					}
				}
				runInAction(() => {
					this.citiesWeather = [...this.citiesWeather, cityData];
				});

			}
		} catch (error) {
			store.notification.setMessageNotification('Error getting information!');
			store.notification.setTypeNotification('error');
			store.notification.openNotification();
			console.log(error);
		}
	}

	@action
	async changeCityWeather(data: ISearchCityData) {
		try {
			const response = await api.get(
				`https://api.openweathermap.org/data/2.5/onecall?lat=${data.lat}&lon=${data.lon}&exclude=minutely,daily&units=${data.units}&appid=5367195d6e5bda5d4093665bfabfc217`
			)
			if (response) {
				const cityData = {...response.data, ...{id: data.id, country: data.country, units: data.units}};
				runInAction(() => {
					this.citiesWeather = this.citiesWeather.map(city => {
						return city.id === data.id ? cityData : city;
					});
				});
			}
		} catch (error) {
			store.notification.setMessageNotification('Error getting information!');
			store.notification.setTypeNotification('error');
			store.notification.openNotification();
			console.log(error);
		}
	}

	@action
	deleteCityWeather(id: number) {
		this.citiesWeather = this.citiesWeather.filter(item => item.id !== id);
	}
}

export default new Weather()