import {observable, action, makeAutoObservable} from 'mobx';
import { runInAction} from "mobx"
import {api} from '../utils/config';

import {IDictionary, LangType} from '../models/language';


class Language {
	@observable language: LangType = 'en';
	@observable dictionary: IDictionary | null = null;

	constructor() {
		makeAutoObservable(this)
	}

	@action
	setLanguage(lang: LangType) {
		this.language = lang;
		localStorage.setItem('weatherAppLanguage', lang);
	}

	@action
	async setDictionary() {
		const response = await api.get(`/i18n/${this.language}.json`);
		runInAction(() => {
			this.dictionary = response.data;
		});
	}

	@action
	async translateText(sourceText: string, targetLang: string) {
		const source = "en";
		const response = await api.get(`https://translate.googleapis.com/translate_a/single?client=gtx&sl=${source}&tl=${targetLang === 'ua' ? 'uk' : targetLang}&dt=t&q=${encodeURI(sourceText)}`);
		if (response.status !== 200) {
			return sourceText;
		}
		return response.data[0][0][0];
	}
}

export default new Language()