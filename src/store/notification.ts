import {observable, action, makeAutoObservable} from 'mobx'

import {NotificationType} from '../models/notification';


class Notification {
	@observable notification: boolean = false;
	@observable messageNotification: string = '';
	@observable typeNotification: NotificationType = 'success';


	constructor() {
		makeAutoObservable(this)
	}

	@action
	openNotification() {
		this.notification = true;
	}

	@action
	closeNotification() {
		this.notification = false;
	}

	@action
	setMessageNotification(message: string) {
		this.messageNotification = message;
	}

	@action
	setTypeNotification(type: NotificationType) {
		this.typeNotification = type;
	}

}

export default new Notification()