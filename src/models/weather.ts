export interface ICity {
	id: number,
	coord: {
		lat: number
		lon: number
	},
	name: string,
	sys: {
		country: string
	}
}

export interface ICityFull extends ICity {
	units: string
}

export interface ISearchCityData {
	id: number,
	lat: number,
	lon: number,
	units: string,
	country: {
		name: string,
		code: string
	}
}

export interface ICityWeather {
	id: number
	lat: number,
	lon: number,
	units: string,
	country: {
		name: string,
		code: string
	},
	current: {
		dt: number,
		feels_like: number,
		temp: number,
		humidity: number,
		pressure: number,
		wind_speed: number
		weather: [{
			description: string,
			main: string
		}]
	},
	hourly: [{
		dt: number
		temp: number
	}]
}

export type OutInfoWeatherType = 'Clouds' | 'Rain' | 'Clear' | 'Snow' | 'Mist';