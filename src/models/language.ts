export type LangType = 'en' | 'ru' | 'ua';

export interface IDictionary {
	search: {
		label: string,
	},
	searchBtn: {
		label: string
	},
	locale: string,
	info: {
		feelsLike: string,
		wind: string,
		humidity: string,
		pressure: string
	},
	outInfo: {
		Clouds: string,
		Rain: string,
		Clear: string,
		Snow: string,
		Mist: string
	}
}